<?php


class Db_Model {
    
    //----- DBに使う変数の宣言 -----
    
    public $db_user = "root";       //ユーザ名
    public $db_pass = "ecc";        //パスワード
    public $db_host = "localhost";  //ホスト名
    public $db_name = "search";     //DB名
    
    //DB操作用のオブジェクト
    public $db_object;
    
    public function __construct() {
        
        //インスタンス化(DBに接続)
        $this->db_object = new mysqli($this->db_host,$this->db_user,$this->db_pass,$this->db_name);
    
        if($this->db_object->connect_error){
            
            die("データベースに接続失敗:".$this->db_object->connect_error);
        }
        
        //文字コード設定
        if(!$this->db_object->set_charset("utf8")){
            
            printf("文字コードの設定失敗 %s\n",$this->db_object->error);
        }
    }
    
    //キーワードを使用して SQL文を作成しDB処理をする
    public function createSQL($keyword){
        
        //---------- SQL文を作成　----------
        
        
        
        //キーワード補正
        if($keyword != ""){
            
            $keyword = "%" . $keyword . "%";
        }

        //SQL文を作成
        if($keyword != ""){
            
            //SQL文の元を作成
            //$sql = "select * from data where site like ?";
            //$sql = "select * from data where site like ?"
            
            $sql = "SELECT * FROM data WHERE site LIKE ?";
        }
        
        //プリペアステートメント    
        $stmt = $this->db_object->prepare($sql);
        
        //パラメータのバインド
        if($keyword != ""){
            
            $stmt->bind_param('s', $keyword);
            
        }
        //sql実行
        $stmt->execute();

        
        //結果を変数にバインド
        $stmt->bind_result($id,$site,$url,$content);
        
        
        $cnt = 0;
        
        //検索結果を配列に格納 ?
        while ($stmt->fetch()){
            
            $results[$cnt] = "<p>\n<a href=" . $url . ">".$site."</a>\n";
            $results[$cnt] .= "<br>\n" . $content . "\n</p>\n";
            $results[$cnt] .= "<div align=\"right\">\n";
            $results[$cnt] .= "<a href=\"update_form.php?id=" . $id . "\">更新\n";
            $results[$cnt] .= "<a href=\"delete.php?id=" . $id . "\">削除\n";
            $results[$cnt] .= "</div>\n";
            $cnt++;
        }
        
        $stmt->close();
        $this->db_object->close();
        
        if(!isset($results)){
            
            $results = "none";
        }
        
        return $results;
    }
    
    
    
}
