<?php
//*****************************
//プログラム名:課題05
//クラス名:ie3a
//主席番号:20
//名前:村田直人
//日付:2016年06月09日
//*****************************
require_once("Smarty_Init.php");

//SESSION開始
session_start();

class Kad05 extends Smarty_Init{
    
    private $message;
    
    //親クラスの呼び出し
    public function __construct() {
        parent::__construct();
    }
    
    //認証を行うメソッド
    public function hanteiUsr(){
        
        //POSTがあるかどうかの確認
        if(isset($_POST["sub"])){
            
            //ユーザの入力した「ID」と[PASS]を取得
            $name = trim(htmlspecialchars($_POST["name"],ENT_QUOTES,"UTF-8"));
            $pass = trim(htmlspecialchars($_POST["pass"],ENT_QUOTES,"UTF-8"));
            
            
            //「User」情報の記述されたファイル名
            define("FILE_NAME","usr.dat");
            
            
            //---------ファイル処理----------
            try {
                
                //ファイルを読み込み形式で開く
                $file = @fopen(FILE_NAME, "r") or die("ファイルオープン失敗");
                
                //ファイルが他ユーザに編集されないようにロック
                flock($file,LOCK_EX);
                
                //ファイルの終わりまで繰り返し
                while (!feof($file)){
                    
                    //一行づつ読み込み
                    $data = fgets($file,1000);
                    
                    //読み込んだユーザ情報を「,」で区切り「ID」と「PASS]分ける
                    list($dusr,$dpass) = explode(",",$data);
                    
                    //----------認証処理----------
                    
                    //「ID」と「PASS」の比較
                    if($name == trim($dusr) && $pass == trim($dpass)){//認証成功
                    
                        //UserNameが空文字でなければ、SESSIONに保存
                        if($name != "")
                            $_SESSION["name"] = $name;
                        
                        //「Pass.php」にReDirect
                        header("Location:Pass.php");
                        //redilect
                        break;
                        
                    }elseif ($name == trim($dusr) && $pass != trim($dpass)) {//PassWord不一致
                    
                        //エラーメッセージを設定
                        $this->message = "パスワードが違います";
                        break;
                        
                    }  else {//UserName,PassWordが不一致
                 
                        //エラーメッセージを設定
                         $this->message = "ユーザ名、パスワードが違います";
                         break;
                    }
                }
                
                //ロック解除
                flock($file, LOCK_UN);

                //ファイルを閉じる
                fclose($file);
            } catch (Exception $ex) {
                
                //エラーメッセージ(Exception)を設定
                $this->message = $ex;
                //ロック解除
                flock($file, LOCK_UN);

                //ファイルを閉じる
                fclose($file);
            }
        }else{
            
            //空文字列をSESSIONに保存
            $_SESSION["name"] = "";
        }
        
        
    }
    
    //テンプレートの表示
    public function dispResult(){
        
        $this->smarty_obj->assign("title","kad05");//タイトルの設定
        $this->smarty_obj->assign("message",$this->message);//エラーの表示
        
        
        $this->smarty_obj->display("kad05.tpl");//テンプレート指定
    }
    
}

//「Kad05」インスタンス化
$obj_soe = new Kad05();

$obj_soe->hanteiUsr();
$obj_soe->dispResult();