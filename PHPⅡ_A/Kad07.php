<?php

date_default_timezone_set("Asia/Tokyo");
require_once("HTTP/Request.php");
require_once("XML/RSS.php");
require_once("XML/Unserializer.php");
require_once("./Smarty_Init.php");

 class Kad07 extends Smarty_Init{
    
    //変数宣言(メンバ変数)
    
    private $c_title;
    private $c_link;
    private $c_description;
    
    public function __construct() {
        parent::__construct();
    }

    //HTTP通信でRSSを取得し、内容をxmlに書き出す
    public function readRSS(){
        
        //----------RSS(URL)を取得--------
        
        //POST Dataが存在するかどうか
//        if(isset($_POST["news"])){
        /*
        if(isset($_POST["sub"])){

            
            //選択したRSSのURLを取得
            $site = trim(htmlspecialchars($_POST["news"]));
            
        }else{
            
            //デフォルトのRSS URL(@IT)
            $site = "http://rss.rssad.jp/rss/itm/1.0/netlab.xml";
        }
         */
        
        $site = "http://rss.rssad.jp/rss/itm/1.0/netlab.xml";

        
        //----------HTTP通信処理----------
        
        //HTTPリクエストのURLを指定して、インスタンス化
      
        $req = &new HTTP_Request($site);
        
        
        
        //プロキシの指定
        $req->setProxy("proxy.ecc.ac.jp","8080","","");
        
        //HTTPリクエストを送信
        $req->sendRequest();
        
        //----------ファイル処理----------
        
        //ファイルを開く
        $fp = fopen("rss/rss.xml", "w");
        
        //ファイルを排他処理
        flock($fp, LOCK_EX);
        
        //読み込んだファイルの内容を変数に格納
        $xml = $req->getResponseBody();
        
        //ファイルの内容をrss.xmlに書き込む
        if(fwrite($fp, $xml) == false){
            
            exit("ファイルの書き込み失敗じゃ～");
        }
        
        //ロック解除
        flock($fp, LOCK_UN);
        
        //ファイルを閉じる
        fclose($fp);
    }
    
    //RSSのパース
    public function anaRSS(){
        
        $max = 7;//表示件数
        $count = 0;//カウンタ
        
        //インスタンス化
        $rss = &new XML_RSS("rss/rss.xml");
        
        //解析
        $rss->parse();
        
        //チャンネル情報の取得
        $channel = $rss->getChannelInfo();
        
        //文字列の変換
        $this->c_title = mb_convert_encoding($channel["title"],"UTF-8","auto");
        $this->c_link = mb_convert_encoding($channel["link"],"UTF-8","auto");
        $this->c_description = mb_convert_encoding($channel["description"],"UTF-8","auto");
    
        //itemの取得
        foreach ($rss->getItems() as $items) {
            
            $i_title = mb_convert_encoding($items["title"], "UTF-8","auto");
            $i_link = mb_convert_encoding($items["link"], "UTF-8","auto");
            $i_desc = mb_convert_encoding($items["description"], "UTF-8","auto");
            
            //初期化
            $this->item_data[$count];
            
            //表示する文字列を作成
            $this->item_data[$count] .= "<td>\n";
            $this->item_data[$count] .= "<a href=\"".$i_link. "\ ";
            $this->item_data[$count] .= "target=\"_new\">□".$i_title."</a>";
            $this->item_data[$count] .= "<br><small>".$i_desc."</small>";
            $this->item_data[$count] .= "</td>\n";
            
            //件数のインクリメント
            $count++;
            
            //7件表示したか
            if($count >= $max){
                
                break;//ループを抜ける
            }
        }//foreachの}
    }//readRSSの}
    
    //表示処理
    public function dispResult(){
        
        //channel情報の定義
        $this->smarty_obj->assign("c_title",$this->c_title);
        $this->smarty_obj->assign("c_link",  $this->c_link);
        $this->smarty_obj->assign("c_description",$this->c_description);
        
        //item情報を定義
         $this->smarty_obj->assign("t_item",$this->item_data);
         
         //ファイル名
         $this->smarty_obj->assign("link",$_SERVER["PHP_SELF"]);
         
         $this->smarty_obj->display("kad07.tpl");
    }
    
    
}

$obj = new Kad07();
$obj->readRSS();
$obj->anaRSS();
$obj->dispResult();

?>