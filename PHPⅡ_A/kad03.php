<?php
//*****************************
//プログラム名:kad03.php
//クラス名:IE3A
//主席番号:20
//名前:村田直人
//日付:2016年05月12日
//*****************************


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Kad03{
    
    protected $sikaku;//試験科目
    protected $ten;//点数
    
    //変数の初期化
    public function __construct($sikaku,$ten) {
        
        $this->sikaku = $sikaku;
        $this->ten = $ten;
    }
    
    //変数を出力
    public function dispMessage(){
        print "受験科目：".$this->sikaku."<br>\n";//資格名出力
        print "点数：".$this->ten."";//点数出力
    }
    
}
  class Kad03Sub extends Kad03{
    
    private $goukaku;//合格点
    
    //変数の初期化
    public function __construct($sikaku,$ten,$goukakuten = 825) {
        parent::__construct($sikaku, $ten);//親クラスの呼び出し
        
        $this->goukaku = $goukakuten;
    }
    
    //合格判定 戻り値:true/false
    public function hanteiPass(){
        //判定
        if($this->goukaku <= $this->ten){
       //if(true){
            return true;//合格
        }else{
            
            return false;//不合格
        }
    }
    
    //合格の結果を出力 戻り値：なし
    public function dispResult(){
        
        $result = "あなたの結果は：";
        
        //判定
        if($this->hanteiPass()){
            
           $result = $result."合格です。";//結果を格納
        }
        else{
           $result = $result."不合格です。";//結果を格納
        }
        
        print "合格点：".$this->goukaku."<br>\n";
        print $result;//結果を出力
    }
    
    //名前を出力 戻り値：なし　staticメソッド
    public static function dispName($name){
        print $name."さん<br>\n";
    }
    
}
?>
<?php

//文字コード設定
header("charset=utf-8");

?>
<html>
    
    <head>
        <title>課題03</title>
        <link rel="stylesheet" href="./css/kad.css"/>
    </head>
    <body>
        <h3>課題03　コンストラクタ・アクセス修飾子・staticメソッド</h3>
        <hr>
        <p>
            <?php
            
            $obj = new Kad03Sub("CCNA",825);
            Kad03Sub::dispName("なかば");
            $obj->dispMessage();
            $obj->dispResult();
            
            
            ?>
        </p>
    </body>
</html>