<?php

date_default_timezone_set('Asia/Tokyo');
require_once("Db_Model.php");
require_once("DispSearch.php");

class Kad10 {
    
    public $dbmobj;
    public $viewobj;
    
    
    //コンストラクタ
    public function __construct() {
     
        //インスタンス化
        $this->dbmobj = new Db_Model();
        $this->viewobj = new DispSearch();
    }
    
    //キーワード取得メソッド
    public function getKeyword(){
        
        //変数の初期化
        $key ="";
        
        
        //(キーワード取得)(タイトル)
        $key = trim(htmlspecialchars( $_POST["title"],ENT_QUOTES,"UTF-8" ));
        
        // % エスケープ
        $key = str_replace("%","\%",$key);
        
        //特殊文字をエスケープ
        $key = $this->dbmobj->db_object->real_escape_string($key);
        
        //戻り値を返す
        return $key;
    }
}

$result = "";
$keys = "";

$obj = new Kad10();

if( isset($_POST["title"]) && $_POST["title"] != ""){
    
    $keys = $obj->getKeyword();
    $result = $obj->dbmobj->createSQL($keys);
}
$obj->viewobj->dispResult($result);