<?php

date_default_timezone_set('Asia/Tokyo');
require_once("./DbInit.php");

class Kad08 extends DbInit{
    
    private $month;//Save the current month of data
    
    //parent constructer call
    public function __construct() {
        parent::__construct();
    
        $tday = getdate();
        
        //save the current month of data
        $this->month = date("Ymd",mktime(0,0,0,$tday["mon"],1,$tday["year"]));
    }
    
    //----------database connection prosess----------
    
    // get sql query result
    public function getSQL(){
        
        //initialization of variables
        $blog_data = "";
        
        //get the query of current month
        $sql = "select * from b_content where b_tm >= ? order by message_id desc";
        
        //prepare statement create
        $stmt = $this->db_object->prepare($sql);
        
        //value of bind
        $stmt->bind_param("s",$this->month);
        
        //sql execute
        $stmt->execute();
        
        //result in the variables
        $stmt->bind_result($message_id,$b_title,
                $b_contents,$b_tm,$b_commtent,$b_trackback);
        
        //for the subscript of the array
        $cnt = 0;
        
        //save in view infomation of array
        while($stmt->fetch()){
            
            //create of date
            $tmp_y = substr($b_tm, 0,4);
            $tmp_m = substr($b_tm, 4,2);
            $tmp_d = substr($b_tm, 6,2);
            
            //initiarization 
            $blog_data[$cnt] = "";
            
            //add the date of the array
            $blog_data[$cnt] .= "<h4 class=\"blog\">". $tmp_m . "-" .
                    $tmp_d . "-" .$tmp_y . "</h4>\n";
            
            //add of the title 
            $blog_data[$cnt] .= "<p class=\"title\">". $b_title . "</p>\n";
            
            //add of the content
            $blog_data[$cnt] .= "<div class=\"contents\">" . $b_contents . "</div>\n";
            
            //comment and tracback
            $blog_data[$cnt] .= "<p align=\"right\">\n<a href=\"".$message_id ."\">comment(" .$b_comment .")</a>&nbsp;<a href=\"Sample11.php\">Trackback(". $b_trackback . ")</a>";
            
            $cnt++;
        }
        
        //database disconnection 
        $stmt->close();
        $this->db_object->close();
        
        return $blog_data;
    }
    
    //Setting of template and variable
    public function viewBlog($b_data){
     
        //contents difinition
        $this->smarty_obj->assign("b_data",$b_data);
        
        //call to template
        $this->smarty_obj->display("kad08.tpl");
    }
}
$obj = new Kad08();
$data= $obj->getSQL();
$obj->viewBlog($data);
