<?php

/**
 * ファイル名：DbFukusyu.php
 *
 * クラス・番号 ie2a20
 * 名前         村田直人
 * 日付         2016/04/14
 *
 */

header("charset=utf-8");//文字コード設定

//DB定数宣言
define("HOST","localhost");
define("USER","root");
define("PASS","ecc");
define("DB","zipdb");


?>
<html>
    <head>
        <title>PHPⅠ復讐</title>
        <link rel="stylesheet" href="./css/kad.css">
    </head>
    <body>
        <h3>PHPⅡ データベース復習(〒検索)</h3>
        <hr>
        <p>
        <form action="DbFukusyu.php" method="post">
            <label>郵便番号：</label>
            <input type="text" name="addr" />
            <input type="submit" name="sub" value="検索" />
        </form>
        </p>
        <p>*「-」はつけずに入力してください</p>
       
        <?php
        
            if(isset($_POST["sub"]) && $_POST[sub] != ""){
                
                //DB接続
                if(!$conn = mysqli_connect(HOST,USER,PASS,DB)){
                    die("DBに接続できません");
                }
                //文字コード設定
                mysqli_set_charset($conn,"utf8");
                
                //キーワード修正
                $keyword = trim(htmlspecialchars($_POST["addr"],ENT_QUOTES,"UTF-8"));        
                
                
                //sqlの特殊文字エスケープ
                $keyword = mysqli_real_escape_string($conn,$keyword);
                
                //%をエスケープ
                $keyword = str_replace("%", "\%", $keyword);
                //SQL条件文
                $text = "where zipcode = ". $keyword;
                
                //SQL文作成
                $sql = "select * from zipdata " . $text;
                
                
                //実行準備
                $stmt = mysqli_prepare($conn, $sql);
                //SQL実行
                mysqli_execute($stmt);
                
                //件数取得準備
                mysqli_stmt_store_result($stmt);
                
                //件数取得
                $count = mysqli_stmt_num_rows($stmt);
                
                
                print "<p>\n";
                print "■検索結果\n<br><br>";
                
                //結果取得
                mysqli_stmt_bind_result($stmt,$code,$addr);
                
                if($count > 0){
                    
                    while(mysqli_stmt_fetch($stmt)){
                        
                        //文字列編集
                        $code = substr($code, 0,3) ."-" . substr($code, 3); 
                       
                        print "郵便番号:". $code . " 住所：".$addr ."<br>\n";
                        
                    }
                    
                }else{
                    print 'なし';
                }
                
                //メモリ解放
                mysqli_stmt_free_result($stmt);
                mysqli_stmt_close($stmt);
                
                //DBを閉じる
                mysqli_close($conn);
            }
        
        ?>
       
    </body>
</html>

