<?php

require_once("Smarty_Init.php");

class Smarty_sample2 extends Smarty_Init{
    
    function __construct() {
        parent::__construct();
    }
    
    function sub_disp(){
        
        $this->smarty_obj->assign("title","Smartyサンプル");
        $this->smarty_obj->assign("class","nk3a");
        $this->smarty_obj->assign("no","9");
        $this->smarty_obj->assign("name","たえなか");
        
        $etc = array("こいで","しょうじ","いまい","朱");
        $this->smarty_obj->assign("etc",$etc);
        
        $this->smarty_obj->display("sample_smarty.tpl");
        
    }
}
$init_obj = new Smarty_sample2();
$init_obj->sub_disp();