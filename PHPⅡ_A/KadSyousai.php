<?php

//*****************************
//プログラム名:KadSyousai.php
//クラス名:IE3A
//主席番号:20
//名前:村田直人
//日付:2016年04月21日
//*****************************

class KadSyousai{
    
    //財布
     var $Saifu;
     
     //コンストラクタ
      function KadSyousai($money){
         
         $this -> Saifu = $money;//値を初期化
     }
    
     //財布の中身を返す
     function getSaifu(){
         
         return $this -> Saifu;//値を返却
     }
     
     //財布の中身を更新
     function updateBuy($nedan){
         
         //財布計算処理
         if(($this -> Saifu )> $nedan){//所持金が値段より高いか
             
             $this->Saifu -= $nedan;//財布から金を抜き取る
             
             return true;//処理実行済み
             
         }else{//条件不一致
             
             return false;//処理不能
         }
     }
     
     //商品名出力
     function printMessage($shina){
         
         print $shina . "購入<br>\n";//購入商品出力
    }
}

