<?php

require_once("KadSyousai.php");

class KadKeisyo extends KadSyousai{

    
    var $card = 0;//カードの金額
    
    //コンストラクタ
    function KadKeisyo($money,$card= 0) {

       $this ->KadSyousai($money);//親クラスを呼び出す
       $this ->card = $card;//変数の初期化
        
    }
    
    //カードの金額を返す
    function getCard(){
        return $this->card;
    
    }
    
    //現金とカードどちらで買うか判定
    function updateBuy2($nedan){
        
        if(!$this->updateBuy($nedan)){//カード購入
            
            $this->card -= $nedan;//カードから金額を抜き出す
            return false;
        }  else {//現金購入
            
            return true;
        }
    }
    
    //現金購入時の表示
    function moneyMessage($shina){
        print $shina."-購入<br>\n";
    }
    
    //カードで購入時の表示
    function cardMessage($shina){
        print $shina."-購入(カード)<br>\n";
    }
    
}

?>

