<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Kad04Model
 *
 * @author 2140031
 */
class Kad04Model {
    //put your code here
    private $goukei;

    //集計メソッド
    public function calcGoukei(){
       
        $this->goukei = 0;//初期化
        
        //ケーキの個数と価格を取得
        for($i = 1;$i <= 3;$i++){
            
            $kakaku[$i] =  trim(htmlspecialchars($_POST["number". $i]));//金額
            
            $kosu[$i] = trim(htmlspecialchars($_POST["cake". $i]));//個数 
            
            
            //商品の入力状態を保存
            if(!preg_match("/^[0-9]+$/",$kosu[$i]) || $kosu[$i] == 0){

                $_SESSION["number".$i] = 0;
            }else{

                $_SESSION["number".$i] = $kosu[$i];//\\
            }
        }
        
        //金額の計算
        for($i = 1;$i <= 3;$i++){
            
            //合計の足しこみ
            $this->goukei += ($kakaku[$i] * $kosu[$i]);
        }
    }
    
    //合計結果を返却するメソッド
    public function getResult(){
        
        //金額を返す
        return $this->goukei;
    }
    
}
