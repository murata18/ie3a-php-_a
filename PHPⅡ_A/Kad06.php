<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once("./Smarty_Init.php");

class Kad06 extends Smarty_Init{
    
    //----------使用する変数の宣言----------
    
    private $c_year;//現在の年
    private $c_mon;//現在の月
    private $c_day;//現在の日
    private $c_days;//当月の日数
    private $first_youbi;//当月の最初の曜日
    private $last_youbi;//当月の最終日の曜日
    private $pre_mon;//前月
    private $n_mon;//翌月
    
    //親クラスのコンストラクタの呼び出し
    public function __construct() {
        parent::__construct();
    }
    
    
    //日付情報の取得
    public function getTimeinfo(){
        
        //クエリの存在確認
        if(isset($_GET["mon"])){
            
            //----------クエリに元ずく日付の作成----------
            
            //getで送られたクエリの取得
            $getdate = trim(htmlspecialchars($_GET["mon"],ENT_QUOTES,"UTF-8"));
            
            $get_y = substr($getdate,0,4);//クエリから年を取り出す
            $get_m = substr($getdate,4,2);//クエリから月を取り出す
            $get_d = substr($getdate,6,2);//クエリから日を取り出す
            
            //日付の作成
            $tday = getdate(mktime(0,0,0, $get_m,$get_d,$get_y));
        }  else {
        
            //現在の日付の取得
            $tday = getdate();
        }
        
        //----------詳細情報の取得----------
        
        $this->c_year = $tday["year"];//年の取得
        $this->c_mon = $tday["mon"];//月の取得
        $this->c_day = $tday["mday"];//日の取得
        
        //当月の日数の取得
        $this->c_days = date("t",  mktime(0,0,0, $this->c_mon,1,$this->c_year));
        
        //当月の最初の曜日
        $this->first_youbi = date("w",  mktime(0,0,0, $this->c_mon,$this->c_year));
        
        //当月の最後の曜日
        $this->last_youbi = date("w", mktime(0,0,0, $this->c_days, $this->c_year));
        
        //前月の取得
        $this->pre_mon = date("Ymd",mktime(0,0,0, $this->c_mon,0,$this->c_year));
        
        //翌月の取得
        $this->n_mon = date("Ymd",mktime(0,0,0, $this->c_mon+1,1,$this->c_year));
        
    }
      
    //カレンダー作成
    public function createCalendar(){

        $youbi = 0;//曜日の変数の宣言・初期化
        $week = 0;//週の変数の宣言・初期化
        $day = 1;//日付の変数の宣言・初期化
        
        //タイトルの文字の作成
        $this->t_year_mon = $this->c_year ."年".$this->c_mon."月";
        
        //カレンダーの最終空白個数を計算して格納
        $cal_last = 6 - $this->last_youbi;
        
        //配列の初期化
        $cal_array[$week] = "";

        //空白の作成
        for($i = 0; $i < $this->first_youbi;$i++){
            
            $cal_array[$week] .= "<td>&nbsp;</td>";
            $youbi++;
        }
        
        //日付表示
        while(checkdate($this->c_mon,$day,$this->c_year)){
            
            //カレンダー改行処理
            if($youbi > 6){

                $youbi = 0;//曜日の初期化
                $week++;
                
                $cal_array[$week] = "";
                $cal_array[$week] .= "<tr align=center>";
            }
            
            //色の指定の為のタグ選択
            if($day == $this->c_day){   //本日
                
                $cal_array[$week] .= "<td class=\"today\">";
                
            }  elseif($youbi == 0){     //日曜日
                
                $cal_array[$week] .= "<td class=\"sunday\">";
                
            }elseif($youbi == 6){       //土曜日
                
                $cal_array[$week] .= "<td class=\"saturday\">";
                
            }else{                      //平日
                
                $cal_array[$week] .= "<td class=\"weekday\">";
            }
            
            $cal_array[$week] .= $day."</td>";//日付と列を閉じるタグの格納
            $day++;//次の日へ
            $youbi++;//次の曜日へ
        }
        
        
        //カレンダー後の空白
        if($cal_last > 0 && $cal_last < 7){
            for($i = 0; $i < $cal_last; $i++){
                $cal_array[$week] .= "<td>&nbsp;</td>";
            }
        }
        
        return $cal_array;
    }
    
    public function viewCalendar($cal){
        
        $this->smarty_obj->assign("title","課題06");
        $this->smarty_obj->assign("t_year_mon",$this->t_year_mon);
        $this->smarty_obj->assign("t_calen",$cal);
        $this->smarty_obj->assign("pre_mon",  $this->pre_mon);
        $this->smarty_obj->assign("n_mon",$this->n_mon);
        //$this->smarty_obj->assign("pre_day",$this->pre);
        //$this->smarty_obj->assign("n_mon",$this->n_mon);
        
        $this->smarty_obj->display("kad06.tpl");
    
        
    }
}

$obj = new Kad06();
$obj->getTimeinfo();
$cal = $obj->createCalendar();
$obj->viewCalendar($cal);