<?php

date_default_timezone_set("Asia/Tokyo");
require_once("./../smarty/Smarty.class.php");
/**
 * Description of Smarty_Init
 *
 * @author 2140031
 */
class Smarty_Init {
    
    public $smarty_obj;
            
    function __construct(){
        
        $this->smarty_obj = new Smarty();
        
        $this->smarty_obj->template_dir = "templates";
        $this->smarty_obj->compile_dir  = "templates_c";
        $this->smarty_obj->config_dir = "config";
    }
}
