<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
date_default_timezone_set('Asia/Tokyo');
require_once("./DbInit.php");

class Kad09 extends DbInit{
    
    //----------変数宣言----------
    
    private $title;
    private $r_message;
    private $message;
    private $write_time;
    private $fname;
    
    private $month;


    public function __construct() {
        parent::__construct();
    }
    
    public function getData(){
        
        //POST通信確認
        if(isset($_POST["sub"])){
            
            //エラー表示用変数
            $err_message = "";
            
            //データ取得
            $this->title = trim(htmlspecialchars($_POST["title"],ENT_QUOTES,"UTF-8"));//タイトル取得
            $this->message = trim(htmlspecialchars($_POST["message"],ENT_QUOTES,"UTF-8"));//メッセージ取得
            
            
            //入力チェック
            if(isset($this->title) && $this->title != ""){
                
                //改行処理
                $this->r_message = str_replace("\r\n", "\n", $this->r_message);
                $this->r_message = str_replace("\r", "\n", $this->r_message);
                $this->r_message = str_replace("\n", "<br>", $this->r_message);
                
                //日付取得
                $this->write_time = date("Ymd");
            }else{
                $err_message = "タイトルを入力してください";
            }
            
        }
        
        return $err_message;
    }
    
    //イメージ表示の為の処理
    private function createTag(){
        
        $this->message = "<div class=\"img\"><img src=\"./images/\" " .
                $this->fname ."\"width=120 height=120></div><br>";
    }

    //アップロード
    public function upFile(){
        
        //アップロード先ディレクトリ
        $updir = "./images/";
        
        //アップロードされたファイル名の取得
        $this->fname = $_FILES["upfile"]["name"];
        
        
        //ファイル移動処理
        if(move_uploaded_file($_FILES["upfile"]["tmp_name"],$updir . $this->fname) == false){
            
            //移動失敗
            $up_message = "アップロード失敗";
        }else{
            
            //移動成功
            $up_message = "アップロード成功";
            $this->createTag();
        }
        
        return $up_message;
    }
    
    public function writeDb(){
        
        //書込みメッセージ
        $this->message .= $this->r_message;
        
        //SQL文の作成
        $sql = "insert into b_content(b_title,b_contents,b_tm,b_comment,b_trackback)".
                "values(?,?,?,?,?)";
        
        if(isset($_POST["sub"])){
            
            //クエリのコンパイル
            $sth = $this->db_object->prepare($sql);
            
            //SQLと値をバインド
            $parm = 0;
            
            //値をステートメントとバインド
            $sth->bind_param("sssii",  $this->title,$this->message,
                    $this->write_time,$parm,$parm);
            
            //クエリ実行
            $sth->execute();
            
            $message = "投稿完了しました";
            
            
            //データベースを閉じる
            $sth->close();
            $this->db_object->close();
            
            return $message;
        }
    }
    //表示結果
    public function dispResult($up_message,$db_message){
        
        $this->smarty_obj->assign("up_mess",$up_message);
        $this->smarty_obj->assign("db_mess",$db_message);
        
        //テンプレート呼び出し
        $this->smarty_obj->display("kad09.tpl");

    }

}
$obj = new Kad09();

if(isset($_POST["sub"])){
    
    $messaage = $obj->getData();
    
    if(is_uploaded_file($_FILES["upfile"]["tmp_name"])){
        
        $up_message = $obj->upFile();
    }
    
    $db_messge = $obj->writeDb();
}
//変数の内容の初期化
if(!isset($up_message)){
    
    $up_message = "";
}
if(!isset($db_messge)){
    
    $db_messge = "";
}
$obj->dispResult($up_message, $db_message);
