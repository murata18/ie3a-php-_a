<?php
//*****************************
//プログラム名:課題05
//クラス名:ie3a
//主席番号:20
//名前:村田直人
//日付:2016年06月09日
//*****************************
require_once("Smarty_Init.php");

session_start();

class Pass extends Smarty_Init{

    //コンストラクタ(親クラスの呼び出し)
    public function __construct() {
        parent::__construct();
    }
    
    //テンプレートを呼び出しデータを渡す
    public function dispPusr(){
        
        $this->smarty_obj->assign("name",$_SESSION["name"]);//タイトルの設定
        
        $this->smarty_obj->display("pass.tpl");//テンプレート指定
    }
}
//「Pass」クラスのインスタンス化
$obj_pass = new Pass();

//テンプレートの表示
$obj_pass->dispPusr();
