
<html>
    
    <head>
        <meta charset="utf-8">
        <title>課題07</title>
        <link rel="stylesheet" href="./css/phpcss.css"/>
    </head>
    <body>
        <h3>課題07　PEARの利用（RSSリーダー）</h3>
        <hr>
        <p>
        <table border=0 cellpading=1 cellspacing=0 bgcolor="#89100a" width="500">
            <tr>
                <td>
                    <a style="font-size: 13pt;" href="{$c_link}" target=_blank>{$c_title}</a>
                    <table border=0 cellpading=3 cellspacing=0 width="100%">
                        <thead>
                        <tr bgcolor="#e9e0d1">
                            <td><small><font color="#891001">{$c_description}</font></small></td>
                        </tr>
                        </thead>
                        <tbody>
                            {foreach item=item_d from=$t_item}
                                <tr bgcolor="#ffffff">
                                    {$item_d}
                                </tr>
                            {/foreach}
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
        </p>
    </body>
</html>
