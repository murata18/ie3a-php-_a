
<html>
    
    <head>
        <title>{$title}</title>
        <link rel="stylesheet" href="./css/phpcss.css"/>
    </head>
    <body>
        <h3>課題06　Smartyの利用2（カレンダー）</h3>
        <hr>
        <p>
        <table border="1" align="center" bordercolor="#656567">
            <caption class="title">{$t_year_mon}</caption>
            <tbody>
                <tr>
                    <th class="sunday" align="center" width="30">Sun</th>
                    <th class="weekday" align="center" width="30">Mon</th>
                    <th class="weekday" align="center" width="30">Thu</th>
                    <th class="weekday" align="center" width="30">Wed</th>
                    <th class="weekday" align="center" width="30">Thr</th>
                    <th class="weekday" align="center" width="30">Fri</th>
                    <th class="weekday" align="center" width="30">Sat</th>
                </tr>
                <tr align="center">
                    
                    <!-- カレンダー -->
                    {foreach item=a_week from=$t_calen}
                        {$a_week}
                    {/foreach}
                </tr>
            </tbody>
        </table>
        <hr>
        <div align="center">
            <a href="../Kad06.php?mon={$pre_mon}">&lt;&lt;前月</a>
            <!--
            <a href="../Kad06.php?mon={$pre_day}">&lt;&lt;Pre day</a>
            <a href="../Kad06.php?mon={$n_day}">Next day&gt;&gt;</a>
            -->
            &nbsp
            <a href="../Kad06.php?mon={$n_mon}">次月&gt;&gt;</a>
        </div>        
        </p>
    </body>
</html>
