
<html>
    
    <head>
        <title>課題09</title>
        <link rel="stylesheet" href="./css/blogcss.css"/>
    </head>
    <body>
        <h3 class="t">課題09　MySQLiクラスの利用2(ブログ書込み)</h3>
        <hr>
        <p>
        <table border=0>
            <form action="Kad09.php" method="post" enctype="multipart/form-data">
                <tbody>
                    <tr>
                        <td class="dform"><label for="title">タイトル:</label></td>
                        <td><input type="text" name="title" class="iform" id="title"></td>
                    </tr>
                    <tr>
                        <td class="dform"><label for="message">コメント:</label></td>
                        <td><textarea rows="10" cols="40" name="message" id="message"></textarea></td>
                    </tr>   
                    <tr>
                        <td class="dform"><label for="upfile">イメージ:</label></td>
                        <td>
                            <input type="file" name="upfile" size="30" class="iform" id="upfile">
                            <input type="hidden" name="MAX_FILE_SIZE" value="2000000">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><font size="2" color="#ff0000">*アップロードするファイルは２Ｍバイトまで</font></td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="submit" name="sub" value="投稿" class="iform" />
                        </td>
                    </tr>
                </tbody>
            </form>
        </table>
        </p>
        <p>{$up_mess}</p>
        <p>{$db_mess}</p>
        <p><a href="Kad08.php">ブログへ</a></p>
    </body>
</html>
