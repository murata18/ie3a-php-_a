<html>
    
    <head>
        <title>課題10</title>
        <link rel="stylesheet" href="./css/blogcss.css"/>
    </head>
    <body>
        <h3 class="t">課題10　MySQLiクラスの利用3(検索)</h3>
        <hr>
        <!-- 入力フォーム -->
        <p>
        <form action="Kad10.php" method="post" >
            
            <input type="text" name="title" />
            <input type="submit" name="sub" value="検索" />
        </form>
        <!--  --------------- 結果の表示 -------------- -->
        
        <!--　検索一致件数 -->
        <p>{$hit_mess}</p>

        <!-- 結果の出力 --> 
        {foreach from=$result_mess item=contents}
            {$contents}
        {/foreach}
    </body>
</html>