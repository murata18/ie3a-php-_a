<?php

//文字コード設定
header("charset=utf-8");

//インポート
require_once('KadKeisyo.php');

//所持金
define("SHOJIKIN", 800);

//名前
define("NAME", "なかば");

//商品
define("TARUTO", 250);
define("WINE", 200);
define("ENGEL", 250);
define("MUSU", 250);
define("FOREST", 320);

?>
<html>
    
    <head>
        <title>課題02</title>
        <link rel="stylesheet" href="./css/kad.css"/>
    </head>
    <body>
        <h3>課題02　クラスの継承</h3>
        <hr>
        <p>
            【<?= NAME ?>の現在の所持金】<br>
            <?= SHOJIKIN ?>
        </p>
        <p>【購入商品】</p>
        <?php
            $i = 0;
            $j = 0;
            $buy[2][10];
             //インスタンス化
            $cake = new KadKeisyo(SHOJIKIN);
            $cardUse = 0;
             //購入(タルト)
             if($cake->updateBuy2(TARUTO)){

                 //表示処理
                 $cake->moneyMessage("タルト・ディアブル");
                 $buy[0][$i] = "タルト・ディアブル";
                 $i++;
                 
             }  else {
                 
                 //表示処理
                 $cake->cardMessage("タルト・ディアブル");
                 $cardUse += TARUTO;
                 $buy[1][$j] = "タルト・ディアブル";
                 $j++;
             }
            
             //購入(ワインゼリー)
             if($cake->updateBuy2(WINE)){

                 //表示処理
                 $cake->moneyMessage("ワインゼリー");
                 $buy[0][$i] = "ワインゼリー";
                 $i++;
             }else{
                 //表示処理
                 $cake->cardMessage("ワインゼリー");
                 $cardUse += WINE;
                 $buy[1][$j] = "ワインゼリー";
                 $j++;
             }

             //購入チョコレートエンゼルケーキ
             if($cake->updateBuy2(ENGEL)){

                 //表示処理
                 $cake->moneyMessage("チョコレートエンゼルケーキ");
                 $buy[0][$i] = "チョコレートエンゼルケーキ";
                 $i++;
                 
             }else{
                 //表示処理
                 $cake->cardMessage("チョコレートエンゼルケーキ");
                 $cardUse += ENGEL;
                 $buy[1][$j] = "チョコレートエンゼルケーキ";
                 $j++;
             }

             //購入フレジエ風苺のムース
             if($cake->updateBuy2(MUSU)){

                //表示処理
                 $cake->moneyMessage("フレジエ風苺のムース");
                 $buy[0][$i] = "フレジエ風苺のムース";
                 $i++;
                 
             }else{
                  //表示処理
                 $cake->cardMessage("フレジエ風苺のムース");
                 $cardUse += MUSU;
                 $buy[1][$j] = "フレジエ風苺のムース";
                 $j++;
             }

             //購入黒い森のケーキ
             if($cake->updateBuy2(FOREST)){

                 //表示処理
                 $cake->moneyMessage("黒い森のケーキ");
                 $buy[0][$i] = "黒い森のケーキ";
             }else{
                  //表示処理
                 $cake->cardMessage("黒い森のケーキ");
                 $cardUse += FOREST;
                 $buy[1][$j] = "黒い森のケーキ";
             }

        ?>
        
      
        <p>
            【購入リスト】<br>
            現金購入：
        <?php
            //購入物(現金)を出力
            foreach ($buy[0] as $value) {
                print $value." ";
            }
        ?>
        <br>カード購入：
        <?php
            //購入物(カード)を出力
            foreach ($buy[1] as $value) {
                print $value." ";
            }
        ?>
        </p>
        <p>【おつり合計】</p>
        <?php
         print $cake->Saifu ."円";
        ?>
        <p>【カード合計】</p>
        <?php
         print $cardUse ."円";
        ?>
    </body>
</html>
