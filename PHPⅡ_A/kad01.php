<?php

//*****************************
//プログラム名:kad01.php
//クラス名:IE3A
//主席番号:20
//名前:村田直人
//日付:2016年04月21日
//*****************************

//文字コード設定
header("charset=utf-8");

//インポート
require_once('KadSyousai.php');

//所持金
define("SHOJIKIN", 800);

//名前
define("NAME", "なかば");

//商品
define("TARUTO", 250);
define("WINE", 200);
define("ENGEL", 250);
define("MUSU", 250);
define("FOREST", 320);


//$kago[];//購入物を格納する変数
?>
<html>
    
    <head>
        <title>課題01</title>
        <link rel="stylesheet" href="./css/kad.css"/>
    </head>
    <body>
        <h3>課題01　クラスの使用</h3>
        <hr>
        <p>
            【<?= NAME ?>の現在の所持金】<br>
            <?= SHOJIKIN ?>
        </p>  
        <p>【購入商品】</p>
            
        <?php
             //インスタンス化
            $cake = new KadSyousai(SHOJIKIN);

             //購入(タルト)
             if($cake->updateBuy(TARUTO)){

                 //表示処理
                 $cake->printMessage("タルト・ディアブル");
                 //購入物格納
                 $kago[] = "タルト・ディアブル";
             }

             //購入(ワインゼリー)
             if($cake->updateBuy(WINE)){

                 //表示処理
                 $cake->printMessage("ワインゼリー");
                 //購入物格納
                 $kago[] = "ワインゼリー";
             }

             //購入チョコレートエンゼルケーキ
             if($cake->updateBuy(ENGEL)){

                 //表示処理
                 $cake->printMessage("チョコレートエンゼルケーキ");
                 //購入物格納
                 $kago[] = "チョコレートエンゼルケーキ";
             }

             //購入フレジエ風苺のムース
             if($cake->updateBuy(MUSU)){

                 //表示処理
                 $cake->printMessage("フレジエ風苺のムース");
                 //購入物格納
                 $kago[] = "フレジエ風苺のムース";
             }

             //購入黒い森のケーキ
             if($cake->updateBuy(FOREST)){

                 //表示処理
                 $cake->printMessage("黒い森のケーキ");
                 //購入物格納
                 $kago[] = "黒い森のケーキ";
             }

        ?>
        
      
        <p>【買ったものリスト】</p>
        <?php
            //購入物を出力
            foreach ($kago as $value) {
                print $value." ";
            }
            print "の".  count($kago) . "を購入しました";
        ?>
        
        <p>【おつり合計】</p>
        <?php
         print $cake->Saifu ."円";
        ?>
    </body>
</html>

