<?php


//文字コード設定
header("charset=utf-8");

//Model,Viewをインポートする
require_once("Kad04Model.php");
require_once("kad04View.php");

//セッションの開始
session_start();

?>
<html>
    
    <head>
        <title>title</title>
        <link rel="stylesheet" href="phpcss.css.css"/>
    </head>
    <body>
        <h3>課題04　MVCモデル</h3>
        <hr>
        <?php
            
            //
            $result = "";//変数初期化
            
            $calc = new Kad04Model();//インスタンス化
            
            if(isset($_POST["sub"])){
                
                $calc->calcGoukei();//集計
                
                $result = resultView($calc->getResult());//結果を取得
                
            }else{
                
                //SESSIONの初期化
                for($i = 1;$i <= 3; $i++){
            
                    $_SESSION["number". $i] = 0;
                }
            }
            
        ?>
        <font color="#89100a">教科書発注システム</font>
        <p>
        <form action="<?= $_SERVER["PHP_SELF"] ?>" method="post">
            <table border="1">
                <!-- 項目名 -->
                <tr align="center"><th>phpto</th><th>name</th><th>price</th><th>piece</th></tr>
                
                <tr>
                    <td><img width='80' height='68' src="./images/cake01.jpg"></td>
                    <td>ブッシュド・ノエル</td>
                    <td>250円</td>
                    <input type="hidden" name="number1" value="250">
                    <td>
                        <input type="text" name="cake1" size="4" value="<?php
                               
                                    
                                    if(isset($_SESSION["hogehoge1"])){
                                        print trim($_SESSION["hogehoge1"]);
                                    }else{
                                        print '0';
                                    }
                               
                        ?>">piece
                    </td>
                </tr>
                
                <tr>
                    <td><img width='80' height='68' src="images/cake02.jpg"></td>
                    <td>シブースド・ノエル</td>
                    <td>200円</td>
                    <input type="hidden" name="number2" value="200" >
                    <td>
                        <input type="text" name="cake2" size="4" value="<?php
                               

                                        if(isset($_SESSION["hogehoge2"])){
                                            print trim($_SESSION["hogehoge2"]);
                                        }else{
                                            print "0";
                                        }
                                   
                        ?>">piece
                    </td>
                </tr>
                
                <tr>
                    <td><img width='80' height='68' src="images/cake03.jpg"></td>
                    <td>イチゴとシブースドケーキ</td>
                    <td>400円</td>
                    <input type="hidden" name="number3" value="400">
                    <td><input type="text" name="cake3" size="4" value="<?php
                            
                                    
                                    if(isset($_SESSION["hogehoge3"])){
                                        print trim($_SESSION["hogehoge3"]);
                                    }else{
                                        print "0";
                                    }
                            
                        ?>">piece
                    </td>
                </tr>
                <tr>
                    <td colspan="4"><button type="submit" name="sub">購入</button></td>
                </tr>
            </table> 
        </form>
    </p>
    <p>
        <?php
            print $result;//結果の表示
        ?>
    </p>
    </body>
</html>

