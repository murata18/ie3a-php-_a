<?php

require_once("./../smarty/Smarty.class.php");

class DispSearch {
    
    
    public $smarty_object;
    
    public function __construct() {
        //インスタンス化
        $this->smarty_object = new Smarty();
        
        
        //ディレクトリ指定
        $this->smarty_object->template_dir = "templates";
        $this->smarty_object->compile_dir  = "templates_c";
        $this->smarty_object->config_dir = "config";
    }
    
    public function dispResult($results){
        
        
        if(isset($results) && $results != ""){
            
            
            if($results != "none"){
                $cnt = count($results);
                $cnts = "<font size=2>".$cnt. "件ヒットしました";
            }else{
                
                $cnts = "該当するもはありません";
                $results = "";
            }
            
            
            //値の設定
            $this->smarty_object->assign("hit_mess",$cnts);
            $this->smarty_object->assign("result_mess",$results);
        }
        
        //テンプレート呼び出し
        $this->smarty_object->display("kad10.tpl");
    }
    
}
