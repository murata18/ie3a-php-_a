<?php

require_once("./../smarty/Smarty.class.php");

class DbInit {
    
    //DB infomation settings data
    protected $db_object;
    private $db_user = "root";//user name
    private $db_pass = "ecc";//user password
    protected $db_host = "localhost";//host name
    private $db_name = "db_blog";//database name
    
    
    //construct create
    public function __construct() {
        
        //mysql instance create
        $this->db_object = new mysqli($this->db_host,$this->db_user,
        $this->db_pass,  $this->db_name);

        //DB connect error
        if($this->db_object->connect){
            
            //error log view
            die("データベースに接続失敗:".$this->db_object->error);
        }
        
        //DB charcter code setting
        if(!$this->db_object->set_charset("utf8")){
            
            //character code setting error log
            printf("文字コードの設定失敗 %s\n",$this->db_object->error);
        }
        
        //----------Smarty Settings----------
        
        //Smarty instance create
        $this->smarty_obj = new Smarty();
        
        //each directory setting
        $this->smarty_obj->template_dir = "./templates";
        $this->smarty_obj->compile_dir = "./templates_c";
        $this->smarty_obj->config_dirr = "./config";
    }
    
}
