<?php

date_default_timezone_set("Asia/Tokyo");
require_once("./../smarty/Smarty.class.php");

$smarty = new Smarty();

$smarty->template_dir = "templates";
$smarty->compile_dir  = "templates_c";
$smarty->config_dir = "config";


$smarty->assign("title","Smartyサンプル");
$smarty->assign("class","nk3a");
$smarty->assign("no","9");
$smarty->assign("name","たえなか");

$etc = array("こいで","しょうじ","いまい","朱");
$smarty->assign("etc",$etc);

$smarty->display("sample_smarty.tpl");